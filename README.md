# Which Platform is Right

<p>Which Platform is Right for You?</p>
<p>&nbsp;</p>
<p>To get your message out to the masses &mdash; to connect with your personal tribe &mdash; you need a platform.&nbsp; I&rsquo;ve found that there are five main types of platforms. Before you start building yours, you should decide which is best for you. A lot of how you build your platform will depend on your personal voice.</p>
<p>&nbsp;</p>
<p>The Journalist</p>
<p>The Journalist builds his platform on asking questions. The only requisite for this type of platform is curiosity.</p>
<p>&nbsp;</p>
<p>When I set out to start this blog, I was nervous. I was no expert on writing, so what right did I have to tell other people how to do it? I didn&rsquo;t even have a published book (yet).</p>
<p>&nbsp;</p>
<p>Then, I found out about Darren Rowse. Darren, as you may know, started one of the largest blogging communities (<a href="https://topessaycompanies.com/best-essay-services/grabmyessay-com/">grabmyessay review</a>) in the world.</p>
<p>&nbsp;</p>
<p>How did he build his platform? Did he wait until he was an expert? No.</p>
<p>&nbsp;</p>
<p>He began his journey by asking questions and sharing the answers publicly.</p>
<p>&nbsp;</p>
<p>Darren&rsquo;s curiosity has attracted hundreds of thousands of people who daily join his journey. He is considered to be a leading expert on blogging. And it all began with asking questions.</p>
<p>&nbsp;</p>
<p>If you are a naturally inquisitive person, this may be an excellent platform for you to build.</p>
<p>&nbsp;</p>
<p>The Prophet</p>
<p>The Prophet builds her platform on telling the truth. The requisite for this type of platform is a passion for authenticity.</p>
<p>&nbsp;</p>
<p>I can think of few people who have done this better than my friend Jamie Wright.</p>
<p>&nbsp;</p>
<p>Jamie authors a popular blog called <a href="https://topessaycompanies.com/best-essay-services/kingessays-com/">kingessays reviews</a>, on which she riffs and rants about faith, life, and other stuff that bugs her. She complains and cusses and often confesses.</p>
<p>&nbsp;</p>
<p>In short, Jamie says all the things missionaries wish they could say. And people love her for it. Ask any of her readers why and they&rsquo;d probably tell you, &ldquo;Because she&rsquo;s real.&rdquo;</p>
<p>&nbsp;</p>
<p>She tells the truth &mdash; the dirty, ugly, beautiful truth.</p>
<p>&nbsp;</p>
<p>Prophets are not always popular. They are unpredictable and usually offending someone.</p>
<p>&nbsp;</p>
<p>Another great example is Seth Godin. Seth is an iconoclast in the business world. He calls out the brokenness of the status quo &mdash; whether it be in marketing, education, or charity work &mdash; and challenges us to something better.</p>
<p>&nbsp;</p>
<p>This is what good prophets do &mdash; not only condemn the dark, but call us into the light.</p>
<p>&nbsp;</p>
<p>The Artist</p>
<p>The Artist builds his platform by creating art &mdash; whether it be music, painting, or entrepreneurship. The requirement is an eye for beauty.</p>
<p>&nbsp;</p>
<p>One of my favorite artists is Jon Foreman, the lead singer of the rock band Switchfoot.</p>
<p>&nbsp;</p>
<p>Jon communicates the truth of his message through the words he sings and the notes he plays. He challenges his listeners through powerful art that causes you to ask questions long after the song is over.</p>
<p>&nbsp;</p>
<p>Artists speak to our hearts, not our minds. They show us &mdash; through their art &mdash; that another world is possible.</p>
<p>&nbsp;</p>
<p>Having sold millions of records, toured the world many times, and appeared on The Tonight Show, it&rsquo;s hard to say that it hasn&rsquo;t worked for Jon and his band.</p>
<p>&nbsp;</p>
<p>The Professor</p>
<p>The Professor builds her platform on facts and information. She does extensive research until she has achieved mastery. Of course, there is always more to learn, but this type of person knows more than most. The only requirements is a longing to learn.</p>
<p>&nbsp;</p>
<p>A great example of someone who has built a platform this way is Jim Collins.</p>
<p>&nbsp;</p>
<p>Jim is well-respected and sought-after speaker and author. He has written Good to Great,Built to Last, and How the Mighty Fall &mdash; all bestselling business books based on extensive research and case studies that he and his team have done. These books are not light reading. They are full of charts and information and case studies.</p>
<p>&nbsp;</p>
<p>The Professor loves data. If you are going to build your expertise this way, you had better love reading, studying, and analyzing (or find a team that does).</p>
<p>&nbsp;</p>
<p>The Celebrity</p>
<p>Perhaps, the oddest type of platform to build (and the most visible) is that of The Celebrity.</p>
<p>&nbsp;</p>
<p>These people are famous because, well, they&rsquo;re famous. A product of a media-saturated culture, celebrities are a need breed of influencers. They woo and scandalize, and we love them, nonetheless. But not everyone can be a celebrity.</p>
<p>&nbsp;</p>
<p>A celebrity earns his audience through charisma.</p>
<p>&nbsp;</p>
<p>Often, the person is good-looking or talented at a particular craft (e.g. acting), but not always. These people, it seems, are famous because, well, they&rsquo;re famous.</p>
<p>&nbsp;</p>
<p>The best example of this type of platform is <a href="https://en.wikipedia.org/wiki/Ashton_Kutcher">Ashton Kutcher</a>. A talented entrepreneur and well-known actor, Ashton has something that makes him especially interesting to his fans and customers. He is charismatic, full of energy, ideas, and excitement. As a result, people love listening to him.</p>
<p>&nbsp;</p>
<p>Networkers would fall into this group, as well. They have influence, because they&rsquo;re good with people. They may not be the up-front-and-center person, but they are charismatic, nonetheless.</p>
<p>&nbsp;</p>

<p>Related resources:</p>
<ul>
<li><a href="https://0gl6q8umlxa.typeform.com/to/AHP9sSJg">Helping Your Teen Conform with School Dress Codes</a></li>
<li><a href="https://patriciahelps.bigcartel.com/product/answering-common-job-interview-questions">ANSWERING COMMON JOB INTERVIEW QUESTIONS</a></li>
<li><a href="https://patriciaholmes.blog.ss-blog.jp/2021-05-29?1622219225">Relying on Cloud-Based Services for Better Business Management</a></li>
<li><a href="https://writings.thebase.in/">Top 3 Branding Mistakes and What You Should Learn from Them</a></li>
</ul>
<p>&nbsp;</p>
<p>&nbsp;</p>
<p>&nbsp;</p>
